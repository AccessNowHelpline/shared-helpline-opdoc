.. this is the index file that tells Sphinx/RtD how to build the static site at https://maggie-documentation.readthedocs.io/en/latest/ .


Access Now Helpline: Operational Documentation
==============================================

Internal Policies
=================

.. toctree::
   :maxdepth: 3

   Record Destruction <2-Internal_Policies/Information_Classification_and_Protection/Record_destruction>
   Record Retention <2-Internal_Policies/Information_Classification_and_Protection/Record_retentions>
   Threat Intelligence Sharing - CiviCERT Report Template <2-Internal_Policies/Threat_Intelligence_Sharing/civicert_report_template>

Case Management
===============

.. toctree::
   :maxdepth: 3

   3-Case_Management/Incident_Handling/Acknowledging_report
   3-Case_Management/Incident_Handling/How_to_report_an_incident
   3-Case_Management/Incident_Handling/CS_Events_and_Incident_Definition


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
