---
title: Record Destruction
tags: [internal_policies, information]
keywords: record destruction, storage media destruction
last updated: 23 January 2020
summary: "How media such as hard drives, portable storage devices etc. are destroyed."
sidebar: mydoc_sidebar
permalink: Record_destruction.html
folder: mydoc
---

Data destruction process
========================

## Physical documents

Physical paper documents containing CONFIDENTIAL information should be destroyed
in a ribbon or cross-cut shredder, while any other physical documents
containing RESTRICTED information must be shredded with a cross-cut shredder.


## Storage devices

Hard drives, USB thumb drives, and other portable storage devices should be
securely erased with a single pass of clearing process: random data (/dev/urandom)
or zeroed data (/dev/zero) before they are thrown out.
Write-once CDs should be broken into pieces or destroyed in the shredder before
being thrown out.


## Digital data

Digital files containing CONFIDENTIAL information can be simply deleted, while
for RESTRICTED data a minimum of one clearing process data must be done over
the file. 
