---
title: Record Retentions
tags: [internal_policies, information]
keywords: record retentions, information retention, data retention, storage duration, backup policy, retention policy, data breach policy
last updated: 23 January 2020
summary: "How long various classifications of information are retained and stored."
sidebar: mydoc_sidebar
permalink: Record_retentions.html
folder: mydoc
---

#### 2.3 Record Retentions

All information restricted to the Digital Security Helpline is stored in the Helpline's own servers, for which the following policy applies:

##### Backup Policy

Backups are stored for a maximum of 30 days. After this period, backups are overwritten. Incremental backups are performed every day. Full backups are performed every 7 days.

All backups are PGP-encrypted, which means that only those with access to the backup PGP key (Access Now's Sysadmin team) are able to decrypt their content.

Backups are synchronized with the storage server via SSH using a key-based authentication.

##### Retention Policy

All information in the Helpline's infrastructure, including requests from clients, personal information on clients and partners, and internal documentation, is stored for as long as necessary in the Helpline's servers, for the delivery of Helpline services and for compliance with national and international legal obligations, including the prevention of criminal offenses and the enforcement of civil law claims. 

All information in the Helpline's infrastructure, except for public information which is non-sensitive and does not include any personal data, is stored on password-protected platforms located either behind the Helpline's VPN or requiring 2-factor authentication, and in work devices with full-disk encryption. This kind of information is only transferred through end-to-end encrypted channels of communication. These are the minimum standards in place and, as they are also evolving, may change in the future. 

The Helpline's infrastructure is only accessible by the Helpline. Information in the Helpline's infrastructure that includes personal data may be shared with:

- The Helpline; Access Now's SysAdmin team; Access Now's Legal team; members of the impacted entity; and “need-to-know” third parties (i.e.  third parties, like partners, other digital security specialists, etc., who need to have access to a certain piece of information restricted to the Helpline in order to perform their role within the intended purpose), for the delivery of Helpline services.
- The Helpline and Access Now's Legal team for compliance with national and international legal obligations, as mentioned above.


##### Data Breach Policy

In case of a personal data breach which is likely to result in a risk to the rights and freedoms of the data subjects, Access Now shall notify the data subjects without undue delay and also notify the supervisory competent authority, without undue delay and where feasible, no later than 72 hours after having become aware of the breach, in accordance with the EU's General Data Protection Regulation.

In addressing data security breaches, Access Now shall take measures to mitigate damage, investigate, conduct remedial action and comply with regulatory requirements for information security.
