---
title: CiviCERT Report Template
tags: [template, internal_policies, threat_intelligence_sharing, CiviCERT]
keywords: Threat intelligence, sharing, CiviCERT, internal policies, report
last updated: 29 June 2021
summary: "Template for monthly CiviCERT reports."
sidebar: mydoc_sidebar
permalink: civicert_report_template.html
folder: mydoc
conf: confidential
---



Hi everyone, 

We are sharing below some stats and trends observed by Access Now's Helpline in the month of XXX.

### Nature of the cases


### Targets


### Concerning issues and trends


### Events published to CiviCERT's MISP
    

### Helpline preventative efforts


