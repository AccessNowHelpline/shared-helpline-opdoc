# Shared Helpline OpDoc

Shared public Helpline Operational Documentation

## How to format content using Sphinx, RST, and Markdown

Sphinx uses RST (ReStructured Text) by default, but our site is setup to also read Markdown. This means that, most of the time, we don't need to change our documentation in order for it to display properly on the site.

However, if we want to edit the Table of Contents for the site, we need to edit the index.rst file and use RST formatting for all the text in there. RST can be a bit confusing, so if you need help with something not listed below, try checking the [official Sphinx documentation for how to format the Table of Contents](https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html#toctree-directive).

### Some important notes about RST:

#### The toctree is the basis of the site

The toctree is what we use to build a sitemap on our RtD site. You can find the official Sphinx documentation for how to format the Table of Contents [here](https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html#toctree-directive).

In order to include a page in the Table of Contents, we need to tell Sphinx that we want it to be included. We do that by editing the index.rst file.

In the index.rst file, you'll see a few sections that start with the following block of text:

````
.. toctree::
   :maxdepth: 3
````

This is the Table of Contents element. You **can** have more than one of these in the index.rst file.

The `.. toctree::` element starts the Table of Contents, and the `:maxdepth: 3` tells it to automatically display three levels of headers from within the documents we list in the Table of Contents.

To list a document in the table of contents, include its name (without the file extension) below the `.. toctree::` block. If the file is not in the same folder as the index.rst file, you will need to list the directory where the file is, too. An example of the formatting would be:

````
.. toctree::
  :maxdepth: 3

  1-Folder/First_document
  1-Folder/Second_document
  2-Folder/Another_document
````

This table of contents will include the files *First_document.md* and *Second_document.md* from the folder *1-Folder*, and the file *Another_document.md* from the folder *2-Folder*. **Notice that the file extensions for these documents are not included in the table of contents.**


#### Normal text does not display in our Table of Contents

If you want to include section headers, you'll need to include them as a 'title' using RST, then include a toctree element with any documents you want to display under that section header.

A title looks like this:

````
My Title
========
````

Note that the `=` characters go exactly to the end of the title text.

Underneath the title, you can include a table of contents by pasting the following text beneath it:
````
.. toctree::
   :maxdepth: 3
````

## How to build the site

The nice thing about using Read the Docs (RtD) to host our documentation is that we don't need to run any build commands on our own computers; all the building is done directly on RtD's servers. In order to build the site, all we need to do is:

1. Update the git repo, making sure that our desired changes are present in the 'main' branch
2. Go to the [RtD project](https://readthedocs.org/projects/public-helpline-opdoc/) and click 'Build version'
3. Once the build has completed, you'll see a green 'Build completed' box above the logs. Click 'View Docs' in the upper right corner to check the site and see if the changes look correct.
