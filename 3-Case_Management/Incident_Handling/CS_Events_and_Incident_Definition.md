---
title: Computer Security Events and Incidents Definition
tags: [case_management, incident_handling]
keywords: computer security events, computer security incidents, definition, evaluation, priority, categories, reports, classification, categorization, classes, topics
last updated: 27 January 2020
summary: "Criteria against which a report can be evaluated to determine if it is an incident and to categorize it."
sidebar: mydoc_sidebar
permalink: CS_Events_and_Incident_Definition.html
folder: mydoc
---

## Computer Security Events and Incidents Definition

### Definition of Computer Security Incident for the CSIRT and Its Constituency

We consider a computer security incident as any malicious event that puts the integrity, confidentiality or availability of the user's data, at risk. In the context of Access Now Digital Security Helpline and its constituency, computer security incidents might also impact the users' physical security or well-being. Access Now Digital Security Helpline's definition of computer incidents includes attacks on the users' data on their own laptops and phones, as well as attacks that affect any digital systems that handle the users' data or activity, like servers of service providers, external storage devices, and other connected objects. 

Access Now Digital Security Helpline does not perform security monitoring activities on the network or devices of its constituents. All events and incidents are reported through the channels created for such purpose.


### Criteria Used to Evaluate Event and Incident Reports

- **Mandate:** For each new case, a mandate checklist must be completed, including especially factors about the requestor or the request that may raise concerns. The checklist is available [in the Helpline's intranet](https://git.accessnow.org/access-now-helpline/documentation/blob/master/internal/access-ds-helpline-operators-manual/operators_manual.md#mandate-checklist) and will be provided on request to auditors. During this process we evaluate the following aspects: 
    - if the requestor is indeed within our constituency;
    - if the request is a digital security issue;
    - if the requested service is among the services the Helpline offers;
    - if the request is related to protecting positive humanitarian work;
    - if the requestor is a non-violent organization or individual;
    - the risk of the case for Access Now and for the requestor;
    - our financial capability to assist.

- **Trust in requestor:**  For every request from a new individual or organization, we perform a vetting process. The purpose of vetting clients is an exercise in reducing and assessing risk. Some of the risks mitigated by vetting include: the risk to Access Now of reputation damage resulting from working with organizations that themselves do not uphold basic human rights, or are controversial for any other reason; the risk of being socially engineered by our adversaries into releasing information, or allowing our adversaries into getting a foothold onto the platforms used by the Helpline; the risk of adversaries consuming our resources in fake incidents, thus denying capability to the people and organizations that really require assistance. Vetting is an exercise of doing adequate due diligence with the clients we assist, to ensure they are truly users at risk. The complete vetting process is available [here](Other_policies/Vetting_Process.md).

- **Priority:** This evaluation parameter relates to a value assigned to each incident. Priority levels allow the Helpline to assign the right amount of resources to each incident, and define the order in which tickets should be resolved. Priority reflects the organizational response required for each request. The priority value is calculated based on the impact and urgency of the incident.

    - *Impact:* An impact level is assigned to each incident by considering the possible consequences and effects of the event and solution to be proposed. The Helpline's impact scales includes three levels: high, moderate, and low. The evaluation of the impact level should also consider the consequences on the organization's reputation. 

    - *Urgency:* The urgency level defines the amount of delay that can be tolerated and how quickly a solution is needed. Incidents can be classified as highly urgent, moderately urgent and not urgent. This will depend of various factors, including the timelines involved, and the level of threat implied in case action is not taken within a certain time frame.


### Incident Categories and Corresponding Priorities

We record 11 incident categories, all described below:

- **Fake Domain:** The client has discovered a malicious domain, impersonating a website owned by the client
- **Vulnerability:** The client has discovered, or was alerted to, a system weakness, or an attacker who can exploit, or has access to, a system flaw
- **Account Compromise:** The client has lost access to their account, and/or suspects or has confirmed malicious activity might be taking place through their account.
- **Censorship:** The case relates to the censorship of the client's web presence, whether this is by technical means, through a takedown notice or seizure of the web content, etc.
- **Data Leak:** The case relates to the mitigation of leaks, unauthorized access to, or publication of sensitive data, as reported by a client.
- **Phishing/Suspicious Email:** This case relates to phishing, spam or spoofing. If the suspicious email includes possible malware or links to a site where malware might be downloaded, the "Malware" category will be used.
- **Shutdown:** The case relates to an internet outage incident.
- **System Compromise:** The case relates to a confirmed compromise on the client’s device. This includes laptops, phones, servers, and network devices
- **DDoS Attack:** Although DDoS attacks are also ways of censoring individuals or organizations, due to the importance of this category we are separating it into a more specific category.
- **Harassment:** The case relates to the client being harassed online or offline.
- **Malware:** The case relates to an attempted malware infection on a device used by the client.

Priority levels are not tied to incident categories. Instead, as explained in the previous section, priority is defined based on the urgency and impact of each incident. This analysis is performed manually by the incident handlers to set adequate priorities considering also the type of the requestor, their country and aspect of work.


#### Correlation and Combination of Reports, Events, and Incidents

Each reported incident is tracked through an unique case ID, which corresponds to a ticket in the ticketing system. When additional reports are received, the incident handler in charge of the request should ensure links exist between the new request and other previous related tickets. We distinguish three types of relationship between tickets:

- **Parental:** According to the Helpline's procedures, each discrete request must be tracked through an individual case. There are occasions where a new request originates from an existing one. In these cases the "child-parent" relationship is used.
- **Dependence:** While working on requests, there will be occasions where a task requires another action to be completed before being able to fully complete the original request. In these cases the "depends on <-> depended on" relationship is used. In order to resolve the original request, all the "depends on" cases must be completed first.
- **Reference:** This relationship is created to mark a relationship, in the type or motivation of the attack, modus operandi, vector of compromise, context, etc. This link is established as informational for the case handlers.



