---
title: Acknowledging Report
tags: [case_management, incident_handling]
keywords: acknowledging report, ticketing system, service-level-agreement
last updated: 27 January 2020
summary: "How the team acknowledges reports."
sidebar: mydoc_sidebar
permalink: Acknowledging_report.html
folder: mydoc
---


Access Digital Security Helpline is a **24/7/365 operation**, with an operator always
ready to respond to any emergency. We operate three 8-hour shifts, from three 
geographic locations roughly time-zone-equidistant around the globe. 
These offices are located in Manila in the Philippines, Tunis in Tunisia, and
San Jose in Costa Rica.

All reports sent to the Helpline's contact email - help@accessnow.org - are recorded
in our ticketing system, which creates a new ticket for each incoming email and
assigns them an identifying number. All incident handlers are notified of new 
reports and the incident handlers on shift respond to incoming cases.

If an incident is not responded within the first 60 minutes after a message is received, an escalation message is sent to the shift leaders. If the incident is still not responded within the first 90 minutes of it's creation, a notification is sent to the shift leaders and the Helpline Director. 

We commit to reply to any new case, informing the client that we are working on
their issue, and to move the status of the relevant ticket from "new" to "open",
within **two hours** of the case ticket being created.
