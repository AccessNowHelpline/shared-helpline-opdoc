---
title: How to report an incident
tags: [case_management, incident_handling]
keywords: incident report, instructions for users, intake mechanism, service-level-agreement, SLA, reporting guidelines
last updated: 27 January 2020
summary: "Incident reporting guidelines."
sidebar: mydoc_sidebar
permalink: How_to_report_an_incident.html
folder: mydoc
---

# How to report an incident

Access Now’s Digital Security Helpline works with individuals and organizations
around the world to keep them safe online. The Helpline supports users at risk to
improve their digital security practices, and provides rapid-response emergency
assistance to users under attack.

The Helpline will walk individuals and organizations through assessing the risks
they face in their work, helps them prioritize their digital security needs,
help them resolve existing problems and recommend digital security best
practices.

A complete list of services can be found
[here](Provided_services.md).

The Helpline will reply to requests in 9 different languages:

|   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|
| ENGLISH | ARABIC | FRENCH | SPANISH | RUSSIAN | PORTUGUESE | GERMAN | FILIPINO | ITALIAN |


## Instructions for users on how to contact the Helpline

These instructions can be found in the [Helpline
page](https://www.accessnow.org/help/) of Access Now's website.


1. **Send an email to the Helpline**

    Send us your request or security question. If you can, use our PGP key. You
    will receive an email confirmation right away.

    - PGP key server: hkps.pool.sks-keyservers.net
    - Fingerprint: 6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC

2. **We follow up with you**

    You will hear from us within two hours of your request.

3. **Secure the conversation**

    We will (1) secure our communications channel with you, and (2) discuss your
    needs.

4. **Confirm your information**

    The first time you reach out to us, we will seek to confirm with trusted
    partners that you are who you are. In particular, we will confirm your email
    address and your organization.

    This is necessary to protect you in case you are being impersonated, and
    ensures that we focus our support on civil society groups, media, and human
    rights defenders.

5. **Get help**

    We will provide you the support you need, which could include referring you
    to another organization to provide the requested service and collaborating
    with the provider to ensure delivery of that service.


### Important recommendations

To ensure users contact us in a safe manner, we recommend they do the following:

- Include only non-sensitive information about their case in their initial email
  to us unless they are using an encrypted communication tool like GPG or
  Signal.
- Add only non-sensitive details in the subject line of the email.
- Prepare ahead. Rather than contact the Helpline only when facing a digital
  security emergency, we recommend users to sert up a secure communications
  channel with us before they face an emergency. Helpline staff can assist them
  in setting up a secure communication channel so that there will be no delay
  when assistance is needed.
- If a user does not have email encryption or another secure communication
  channel configured, they can just send an email to help@accessnow.org stating
  that they need assistance. The Helpline's incident handlers will help create a
  secure communications channel so as to discuss with the user the more
  sensitive details of their needs.
- In some countries, reaching out for digital security assistance may be risky.
  If a user is concerned about this, we recommend users to ensure that their
  email provider uses Transport Layer Security (TLS). This protects information
  such as the “To” address and email content from third parties outside the
  email provider, offering some protection in reaching out to us.

*See also our [Intermediary process](2-Policies/Other_policies/Intermediary_Process.md).*
